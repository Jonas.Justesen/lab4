package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid
{
    private int rows;
    private int cols;
    private Color[][] grid;
    private List<CellColor> cells = new ArrayList<>();

    public ColorGrid(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        this.grid = new Color[rows][cols];
    }

    @Override
    public int rows(){
        return this.rows;
    }

    @Override
    public int cols(){
        return this.cols;
    }

    @Override
    public Color get(CellPosition pos){
        return this.grid[pos.row()][pos.col()];
    }

    @Override
    public void set(CellPosition pos, Color color) {
        this.grid[pos.row()][pos.col()] = color;;
    }

    @Override
    public List<CellColor> getCells() {
      for (int i = 0; i < this.rows; i++) {
        for (int j = 0; j < this.cols; j++) {
          this.cells.add(new CellColor(new CellPosition(i, j), this.grid[i][j]));
        }
      }
      return this.cells;
    }
}
