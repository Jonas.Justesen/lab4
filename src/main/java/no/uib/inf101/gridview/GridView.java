package no.uib.inf101.gridview;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.IColorGrid;

public class GridView extends JPanel{
// Field variables
  private IColorGrid grid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;
  private static final double MARGIN = 30;

  // Constructor
  public GridView(IColorGrid grid) {
    this.setPreferredSize(new Dimension(400, 300));
    this.grid = grid;
  }

  @Override
  public void paintComponent(Graphics g) {
    // Draw grid
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);
    
  }
  
  /**
   * Draws the grid on the canvas.
   * @param g2 the graphics object to draw on
   */
  public void drawGrid(Graphics2D g2) {
    // Draw background
    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;
    g2.setColor(MARGINCOLOR);
    g2.fill(new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, width, height));

    // Draw cells
    CellPositionToPixelConverter cellPosition = new CellPositionToPixelConverter(new Rectangle2D.Double(OUTERMARGIN, OUTERMARGIN, width, height), grid, MARGIN);
    drawCells(g2, grid, cellPosition);
  }

  /** 
   * Iterates through the cells in the grid and draws them on the canvas.
   * @param g2 the graphics object to draw on
   * @param cells the cells to draw, mainly used to gather cell.color
   * @param cellPosition the position of the cell
  */
  private static void drawCells(Graphics2D g2, CellColorCollection cells, CellPositionToPixelConverter cellPosition) {
    for (CellColor cell : cells.getCells()) {
      if (cell.color() != null) {
        g2.setColor(cell.color());
      } else {
        g2.setColor(Color.DARK_GRAY);
      }
      g2.fill(cellPosition.getBoundsForCell(cell.cellPosition()));
    }
  }
}
